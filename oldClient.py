import socket
import json
SERVER_IP = "127.0.0.1"
SERVER_PORT = 8826

def send_msg(sock, msg, code):
    b = code[0].encode() + str(len(msg)).zfill(4).encode() + msg.encode()
    sock.send(b)

with socket.socket() as sock:
    sock.connect((SERVER_IP, SERVER_PORT))
    roomId = 0
    while True:
        code = input('Choose type:\n1) login\n2) sign up\n3) sign out\n4) get rooms\n5) get players\n6) join room\n7) create room\n')
        data = ''
        if code == '1':
            data = json.dumps(
                {
                    'username': input('Enter username: '),
                    'password': input('Enter password: ')
                }
            )
        elif code == '2':
            data = json.dumps(
                {
                    'username': input('Enter username: '),
                    'password': input('Enter password: '),
                    'email': input('Enter email: ')
                }
            )
        elif code == '3' or code == '4':
            data = ''
        elif code == '5':
            data = json.dumps( {'roomId': roomId } )
        elif code == '6':
            roomId = int(input('Enter room ID: '))
            data = json.dumps( {'roomId': roomId} )
        elif code == '7':
            data = json.dumps(
                {
                    'roomName' : input('Enter room name: '),
                    'maxUsers' : int(input('Enter max users: ')),
                    'questionCount' : int(input('Enter question count: ')),
                    'answersTimeout' : int(input('Enter answers timeout: ')),
                }
            )
        else:
            data = input('enter message: ')

        send_msg(sock, data, code)
        response = json.loads(sock.recv(1024).decode())
        if code == '7':
            roomId = response['status']
        print('Server response: ', response)
