#pragma once
#include "Room.h"
#include "LoggedUser.h"
#include <map>

class Room;
class LoggedUser;

class RoomManager {
private:
	std::map<int, Room> m_rooms;
public:
	RoomManager() = default;
	~RoomManager() = default;
	void createRoom(LoggedUser user, RoomData roomData);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	std::vector<RoomData> getRooms();
	Room* getRoom(int ID);
};