#include "LoginRequestHandler.h"
#include <iostream>

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& handlerFactory) : m_handlerFactory(handlerFactory) {

}

bool LoginRequestHandler::isRequestRelevant(RequestInfo info) {
	return info.id == RequestIds::LoginId || info.id == RequestIds::SignUpId;
}
RequestResult LoginRequestHandler::handleRequest(RequestInfo info) {
	return info.id == RequestIds::LoginId ? login(info) : signUp(info);
}

RequestResult LoginRequestHandler::login(RequestInfo info) {
	LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);
	LoggedUser* user = m_handlerFactory.getLoginManager().login(loginRequest.username, loginRequest.password);
	IRequestHandler* newRequest = this;
	if (user != nullptr) {
		newRequest = m_handlerFactory.createMenuRequestHandler(user);
	}
	return { 
		JsonResponsePacketSerializer::serializeResponse(LoginResponse{ user != nullptr } ), 
		newRequest 
	};
}
RequestResult LoginRequestHandler::signUp(RequestInfo info) {
	SignUpRequest signUpRequest = JsonRequestPacketDeserializer::deserializeSignUpRequest(info.buffer);
	LoggedUser* user = m_handlerFactory.getLoginManager().signUp(signUpRequest.username, signUpRequest.password, signUpRequest.email);
	IRequestHandler* newRequest = this;
	if (user != nullptr) {
		newRequest = m_handlerFactory.createMenuRequestHandler(user);
	}
	return { 
		JsonResponsePacketSerializer::serializeResponse( SignUpResponse{ user != nullptr } ), 
		newRequest 
	};
}