#pragma once
enum RequestIds {
	LoginId = 1,
	SignUpId = 2,

	SignOutId = 3,
	GetRoomsId = 4,
	GetPlayersId = 5,
	JoinRoomId = 6,
	CreateRoomId = 7,

	CloseRoomId = 8,
	StartGameId = 9,
	GetRoomStateId = 10,
	LeaveRoomId = 11,

	GetQuestionId = 12,
	SubmitAnswerId = 13,
	GetGameResultsId = 14,
	LeaveGameId = 15,
};