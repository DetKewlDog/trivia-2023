#include "JsonResponsePacketSerializer.h"

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse response) {
	std::string data = json{ {"error", response.message} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignUpResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response) {
	json json;
	json["status"] = response.status;
	json["winsCount"] = response.winsCount;
	json["totalGamesCount"] = response.totalGamesCount;
	JsonResponsePacketSerializer::serializeRoomDataList(response.rooms, json);
	std::string data = json.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response) {
	std::string data = json{ {"players", response.players} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response) {
	std::string data = json{ 
		{"status", response.status},
		{"hasGameBegun", response.hasGameBegun},
		{"players", response.players},
		{"questionCount", response.questionCount},
		{"answerTimeout", response.answerTimeout},
		{"maxPlayers", response.maxPlayers}
	}.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse response) {
	std::string data = json{ {"status", response.status} }.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse response) {
	std::string data = json{ 
		{"status", response.status},
		{"question", response.question},
		{"answers", response.answers}
	}.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse response) {
	std::string data = json{ 
		{"status", response.status},
		{"correctAnswerId", response.correctAnswerId}
	}.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse response) {
	json json;
	json["status"] = response.status;
	JsonResponsePacketSerializer::serializePlayerResultsList(response.results, json);
	std::string data = json.dump();
	return std::vector<unsigned char>(data.begin(), data.end());
}

void JsonResponsePacketSerializer::serializeRoomDataList(std::vector<RoomData> lst, json& j) {
	for (auto room : lst) {
		j["rooms"].push_back(json{
			{"id", room.id},
			{ "name", room.name },
			{ "maxPlayers", room.maxPlayers },
			{ "numOfQuestionsInGame", room.numOfQuestionsInGame },
			{ "timePerQuestion", room.timePerQuestion },
			{ "isActive", room.isActive  }
		});
	}
}

void JsonResponsePacketSerializer::serializePlayerResultsList(std::vector<PlayerResults> lst, json& j) {
	for (auto result : lst) {
		j["results"].push_back(json{
			{ "username", result.username },
			{ "correctAnswerCount", result.correctAnswerCount },
			{ "wrongAnswerCount", result.wrongAnswerCount },
			{ "averageAnswerTime", result.averageAnswerTime }
		});
	}
}