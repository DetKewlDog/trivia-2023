#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "LoggedUser.h"
class Communicator;
class RoomManager;
class LoggedUser;
class IRequestHandler;
class RequestHandlerFactory;

class RoomAdminRequestHandler : public IRequestHandler {
public:
	RoomAdminRequestHandler(LoggedUser* user, Room* room, RequestHandlerFactory& handlerFactory);
	virtual ~RoomAdminRequestHandler();

	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult handleRequest(RequestInfo info) override;
private:
	Room* m_room;
	RequestHandlerFactory& m_handlerFactory;
	LoggedUser* m_user;
	RoomManager& m_roomManager;

	virtual RequestResult closeRoom(RequestInfo info);
	virtual RequestResult startGame(RequestInfo info);
	virtual RequestResult getRoomState(RequestInfo info);
};