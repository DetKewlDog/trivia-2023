#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser* user, RequestHandlerFactory& handlerFactory) : m_handlerFactory(handlerFactory), m_user(user), m_roomManager(handlerFactory.getRoomManager()) {

}

MenuRequestHandler::~MenuRequestHandler() {
	this->m_handlerFactory.m_loginManager.logout(this->m_user->getUsername());
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo info) {
	return info.id == RequestIds::SignOutId 
		|| info.id == RequestIds::GetRoomsId
		|| info.id == RequestIds::GetPlayersId 
		|| info.id == RequestIds::CreateRoomId 
		|| info.id == RequestIds::JoinRoomId;
}
RequestResult MenuRequestHandler::handleRequest(RequestInfo info) {
	if (info.id == RequestIds::SignOutId) return signOut(info);
	if (info.id == RequestIds::GetRoomsId) return getRooms(info);
	if (info.id == RequestIds::GetPlayersId) return getPlayersInRoom(info);
	if (info.id == RequestIds::JoinRoomId) return joinRoom(info);
	return createRoom(info);
}
RequestResult MenuRequestHandler::signOut(RequestInfo info) {
	IRequestHandler* newRequest = this;
	if (m_handlerFactory.getLoginManager().logout(this->m_user->getUsername())) {
		newRequest = m_handlerFactory.createLoginRequestHandler();
	}
	return { 
		JsonResponsePacketSerializer::serializeResponse(LoginResponse{ 1 }), 
		newRequest 
	};
}
RequestResult MenuRequestHandler::getRooms(RequestInfo info) {
	std::pair<unsigned int, unsigned int> stats = m_handlerFactory.m_database->getUserStatistics(m_user->getUsername());

	return {
		JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse{ 1, m_handlerFactory.getRoomManager().getRooms(), stats.first, stats.second }),
		this 
	};
}
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info) {
	GetPlayersInRoomRequest request = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(info.buffer);
	Room* room = m_handlerFactory.getRoomManager().getRoom(request.roomId);
	return {
		room != nullptr ?
			JsonResponsePacketSerializer::serializeResponse({ room->getAllUsers() }) :
			JsonResponsePacketSerializer::serializeResponse(ErrorResponse { "You are not in a room!" }),
		this
	};
}
RequestResult MenuRequestHandler::joinRoom(RequestInfo info) {
	JoinRoomRequest request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);
	bool success = false;
	Room* room = m_handlerFactory.getRoomManager().getRoom(request.roomId);
	IRequestHandler* newRequest = this;
	if (room != nullptr) {
		success = true;
		room->addUser(*this->m_user);
		newRequest = m_handlerFactory.createRoomMemberRequestHandler(this->m_user, room);
	}
	return { 
		JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse{ success }), 
		newRequest
	};
}
RequestResult MenuRequestHandler::createRoom(RequestInfo info) {
	CreateRoomRequest request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);
	unsigned int roomId = m_handlerFactory.getRoomManager().getRooms().size() + 1;
	m_handlerFactory.getRoomManager().createRoom(*this->m_user, { roomId, request.roomName, request.maxUsers, request.questionCount, request.answersTimeout, false });
	Room* room = m_handlerFactory.getRoomManager().getRoom(roomId);
	return {
		JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse{ roomId }),
		m_handlerFactory.createRoomAdminRequestHandler(this->m_user, room)
	};
}