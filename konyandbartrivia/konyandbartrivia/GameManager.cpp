#include "GameManager.h"

GameManager::GameManager(IDatabase* database) {
	this->m_database = database;
}

void GameManager::createGame(Room room) {
	RoomData metadata = room.getMetadata();
	Game game = Game(m_database->getQuestions(metadata.numOfQuestionsInGame), room.m_users, metadata.id, metadata.timePerQuestion);
	for (auto& i : m_games) {
		if (i.m_gameId == game.m_gameId) return;
	}
	m_games.push_back(game);
}
Game* GameManager::getGame(unsigned int gameId) {
	Game game = Game(std::vector<Question>(), std::vector<LoggedUser>(), gameId, 0);
	for (auto& i : m_games) {
		if (i.m_gameId == game.m_gameId) return &i;
	}
	return nullptr;
}
void GameManager::deleteGame(unsigned int gameId) {
	Game game = Game(std::vector<Question>(), std::vector<LoggedUser>(), gameId, 0);
	auto iter = m_games.end();
	for (auto it = m_games.begin(); it != m_games.end(); ++it) {
		if (it->m_gameId == game.m_gameId) {
			iter = it;
			break;
		}
	}
	if (iter != m_games.end()) m_games.erase(iter);
}