#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "LoggedUser.h"
#include "PlayerResults.h"

class Communicator;
class RoomManager;
class LoggedUser;
class IRequestHandler;
class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler {
public:
	RoomMemberRequestHandler(LoggedUser* user, Room* room, RequestHandlerFactory& handlerFactory);
	virtual ~RoomMemberRequestHandler();

	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult handleRequest(RequestInfo info) override;
private:
	Room* m_room;
	RequestHandlerFactory& m_handlerFactory;
	LoggedUser* m_user;
	RoomManager& m_roomManager;

	virtual RequestResult leaveRoom(RequestInfo info);
	virtual RequestResult getRoomState(RequestInfo info);
};