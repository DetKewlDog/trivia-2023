#pragma once
#include <iostream>
#include <thread>
#include <string>
#include "Communicator.h"
#include "SqliteDatabase.h"
#include "RequestHandlerFactory.h"

class Communicator;
class RequestHandlerFactory;
class IDatabase;
class SqlDatabase;

class Server {
protected:
	int _port;
	IDatabase* m_database;
	RequestHandlerFactory m_handlerFactory;

	Communicator* m_communicator;
public:
	Server(int port);
	virtual ~Server();
	virtual void run();
};