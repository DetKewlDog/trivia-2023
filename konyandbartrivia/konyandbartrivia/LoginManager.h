#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>

class LoginManager {
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
public:
	LoginManager(IDatabase* database);
	virtual ~LoginManager() = default;

	virtual LoggedUser* signUp(std::string username, std::string password, std::string email);
	virtual LoggedUser* login(std::string username, std::string password);
	virtual bool logout(std::string username);
};