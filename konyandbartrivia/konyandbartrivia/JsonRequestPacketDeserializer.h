#pragma once
#include "Requests.h"
#include <vector>
#include <fstream>
#include <iostream>
#include "Question.h"

class JsonRequestPacketDeserializer {
public:
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignUpRequest deserializeSignUpRequest(std::vector<unsigned char> buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer);

	static std::vector<std::string> deserializeStringVector(std::string vector);
};