#pragma once
#include "RequestInfo.h"
#include "RequestResult.h"
#include "RequestIds.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

class IRequestHandler {
public:
	virtual bool isRequestRelevant(RequestInfo info) = 0;
	virtual RequestResult handleRequest(RequestInfo info) = 0;
	std::string name() const { return typeid(*this).name(); }
};