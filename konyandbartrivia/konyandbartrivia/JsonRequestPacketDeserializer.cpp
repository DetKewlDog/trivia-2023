#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"

using json = nlohmann::json;

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer) {
	json data = json::parse(buffer);
	return { data["username"], data["password"] };
}
SignUpRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(std::vector<unsigned char> buffer) {
	json data = json::parse(buffer);
	return { data["username"], data["password"], data["email"] };
}
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer) {
	json data = json::parse(buffer);
	return { data["roomId"] };
}
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer) {
	json data = json::parse(buffer);
	return { data["roomId"] };
}
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer) {
	json data = json::parse(buffer);
	return { data["roomName"], data["maxUsers"], data["questionCount"], data["answersTimeout"] };
}
SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer) {
	json data = json::parse(buffer);
	return { data["answerId"] };
}

std::vector<std::string> JsonRequestPacketDeserializer::deserializeStringVector(std::string vector) {
	json data = json::parse(vector);
	return std::vector<std::string>(data.begin(), data.end());
}
