#include "Helper.h"
#include <sstream>

RequestInfo Helper::receiveData(const SOCKET sc) {
	int code = getIntPartFromSocket(sc, 2);
	int dataLength = getIntPartFromSocket(sc, 4);
	if (dataLength == 0) return { code, std::vector<unsigned char>() };
	std::string message = getPartFromSocket(sc, dataLength);
	std::vector<unsigned char> buffer(message.begin(), message.end());
	return { code, buffer };
}

int Helper::getIntPartFromSocket(const SOCKET sc, const int bytesNum) {
	return atoi(getPartFromSocket(sc, bytesNum).c_str());
}

void Helper::sendData(const SOCKET sc, const std::string message) {
	if (send(sc, message.c_str(), message.size(), 0) == INVALID_SOCKET) {
		throw std::exception("Error while sending message to client");
	}
}
std::string Helper::getPartFromSocket(const SOCKET sc, const int bytesNum) {
	char* data = new char[bytesNum + 1];
	if (recv(sc, data, bytesNum, 0) == INVALID_SOCKET) {
		throw std::exception("User has disconnected");
	}
	data[bytesNum] = 0;
	std::string received(data);
	delete[] data;
	return received;
}