#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include "Question.h"

class Server;
class Communicator;

class IDatabase {
public:
	virtual ~IDatabase() = default;
	virtual bool open() = 0;
	virtual bool close() = 0;
	
	virtual int doesUserExist(std::string username) = 0;
	virtual int doesPasswordMatch(std::string username, std::string password) = 0;
	virtual int addNewUser(std::string username, std::string password, std::string email) = 0;

	virtual std::vector<Question> getQuestions(int count) = 0;
	virtual std::pair<int, int> getUserStatistics(std::string username) = 0;
	virtual int setUserStatistics(std::string username, int winsCount, int totalGamesCount) = 0;
};