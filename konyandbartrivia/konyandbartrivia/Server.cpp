#include "Server.h"

Server::Server(int port) : m_handlerFactory(0) {
	this->_port = port;
	this->m_database = new SqliteDatabase();
	this->m_handlerFactory = RequestHandlerFactory(this->m_database);
	this->m_communicator = new Communicator(port, this->m_handlerFactory);
}
Server::~Server() {
	delete this->m_communicator;
}
void Server::run() {
	std::thread t_connector(&Communicator::startHandleRequests, this->m_communicator);
	t_connector.detach();
	std::string input;
	std::cout << "Server initialized. Use EXIT to force stop." << std::endl;
	do {
		std::cin >> input;
	} while (input != "EXIT");
}