#include "Room.h"

void Room::addUser(LoggedUser user) {
	if (m_users.size() >= m_metadata.maxPlayers) return;
	auto it = std::find(m_users.begin(), m_users.end(), user);
	if (it != m_users.end()) return;
	this->m_users.push_back(user);
}
void Room::removeUser(LoggedUser user) {
	auto it = std::find(m_users.begin(), m_users.end(), user);
	if (it == m_users.end()) return;
	this->m_users.erase(it);
}
std::vector<std::string> Room::getAllUsers() {
	std::vector<std::string> res;
	for (auto user : m_users) {
		res.push_back(user.getUsername());
	}
	return res;
}
RoomData& Room::getMetadata() {
	return this->m_metadata;
}