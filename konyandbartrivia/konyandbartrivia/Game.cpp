#include "Game.h"

Game::Game(std::vector<Question> questions, std::vector<LoggedUser> users, unsigned int gameId, unsigned int answerTimeout) : m_questions(questions), m_gameId(gameId), m_answerTimeout(answerTimeout) {
	std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
	for (auto user : users) {
		m_players.insert({
			user,
			GameData(now)
		});
	}
}
std::pair<Question, int> Game::getQuestionForUser(LoggedUser user) {
	auto currentTime = std::chrono::steady_clock::now();

	std::map<LoggedUser, GameData>::iterator data = m_players.end();
	for (auto it = m_players.begin(); it != m_players.end(); ++it) {
		if (it->first.getUsername() != user.getUsername()) continue;
		data = it;
		break;
	}
	if (data == m_players.end()) throw std::exception((user.getUsername() + " was nullptr").c_str());
	std::chrono::duration<double> elapsedSeconds = currentTime - data->second.startTimestamp;

	if (elapsedSeconds.count() > 0.2 
		&& data->second.currentQuestion.getQuestion() != "" 
		&& this->m_answerTimeout - elapsedSeconds.count() > 0) {
		std::cout << user.getUsername() << " " << this->m_answerTimeout - elapsedSeconds.count() << std::endl;
		std::this_thread::sleep_for(std::chrono::duration<double>(this->m_answerTimeout - elapsedSeconds.count()));
	}
	data->second.currentQuestion = this->m_questions[++(data->second.currentQuestionId)];
	data->second.startTimestamp = std::chrono::steady_clock::now();
	return { data->second.currentQuestion, data->second.currentQuestionId };
}
bool Game::submitAnswer(LoggedUser user, unsigned int answerId) {
	std::map<LoggedUser, GameData>::iterator data = m_players.end();
	for (auto it = m_players.begin(); it != m_players.end(); ++it) {
		if (it->first.getUsername() != user.getUsername()) continue;
		data = it;
		break;
	}
	if (data == m_players.end()) throw std::exception((user.getUsername() + " was nullptr").c_str());
	bool isCorrect = data->second.currentQuestion.getCorrectAnswerId() == (int)answerId;
	(isCorrect ? data->second.correctAnswerCount : data->second.wrongAnswerCount)++;

	auto currentTime = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsedSeconds = currentTime - data->second.startTimestamp;
	data->second.averageAnswerTime = (data->second.averageAnswerTime + elapsedSeconds.count()) / 2;
	return isCorrect;
}
void Game::removePlayer(LoggedUser user) {
	std::map<LoggedUser, GameData>::iterator data = m_players.end();
	for (auto it = m_players.begin(); it != m_players.end(); ++it) {
		if (it->first.getUsername() != user.getUsername()) continue;
		data = it;
		break;
	}
	if (data == m_players.end()) return;
	this->m_players.erase(data);
}
std::vector<PlayerResults> Game::getPlayerResults() {
	std::vector<PlayerResults> results;
	for (auto player : this->m_players) {
		results.push_back(PlayerResults{
			player.first.getUsername(),
			player.second.correctAnswerCount,
			player.second.wrongAnswerCount,
			player.second.averageAnswerTime
		});	
	}
	return results;
}
bool Game::operator==(const Game& other) const {
	return m_gameId == other.m_gameId;
}