#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "GameManager.h"
#include "Game.h"
class Communicator;
class GameManager;
class Game;
class IRequestHandler;
class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler {
public:
	GameRequestHandler(Game* game, LoggedUser* user, RequestHandlerFactory& handlerFactory);
	virtual ~GameRequestHandler();

	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult handleRequest(RequestInfo info) override;
private:
	Game* m_game;
	LoggedUser* m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFactory;
	Question m_question;

	virtual RequestResult getQuestion(RequestInfo info);
	virtual RequestResult submitAnswer(RequestInfo info);
	virtual RequestResult getGameResults(RequestInfo info);
	virtual RequestResult leaveGame(RequestInfo info);
	virtual RequestResult getRoomState(RequestInfo info);
};