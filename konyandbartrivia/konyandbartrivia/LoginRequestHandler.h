#pragma once
#include "IRequestHandler.h"
#include "Responses.h"
#include "RequestHandlerFactory.h"

class Communicator;
class LoggedUser;
class IRequestHandler;
class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler {
public:
	LoginRequestHandler(RequestHandlerFactory& handlerFactory);
	virtual ~LoginRequestHandler() = default;

	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult handleRequest(RequestInfo info) override;
private:
	RequestHandlerFactory& m_handlerFactory;

	virtual RequestResult login(RequestInfo info);
	virtual RequestResult signUp(RequestInfo info);
};