#pragma once
#include "IDatabase.h"
#include "Room.h"
#include "Game.h"
#include <list>
#include <iostream>

class GameManager {
private:
	IDatabase* m_database;
	std::list<Game> m_games;
public:
	GameManager(IDatabase* database);
	virtual ~GameManager() = default;

	virtual void createGame(Room room);
	virtual Game* getGame(unsigned int gameId);
	virtual void deleteGame(unsigned int gameId);
};