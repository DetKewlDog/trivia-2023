#pragma once
#include <vector>

typedef struct RequestInfo {
	int id;
	std::vector<unsigned char> buffer;
} RequestInfo;