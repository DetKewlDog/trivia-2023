#pragma once
#include "SqliteDatabase.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "GameManager.h"

class IDatabase;
class IRequestHandler;
class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;
class LoginManager;
class RoomManager;
class GameManager;

class RequestHandlerFactory {
public:
	LoginManager m_loginManager;
	IDatabase* m_database;
	RoomManager m_roomManager;
	GameManager m_gameManager;

	RequestHandlerFactory(IDatabase* database);
	virtual ~RequestHandlerFactory() = default;

	virtual LoginRequestHandler* createLoginRequestHandler();
	virtual MenuRequestHandler* createMenuRequestHandler(LoggedUser* user);
	virtual RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser* user, Room* room);
	virtual RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser* user, Room* room);
	virtual GameRequestHandler* createGameRequestHandler(Game* game, LoggedUser* user);
	virtual LoginManager& getLoginManager();
	virtual RoomManager& getRoomManager();
	virtual GameManager& getGameManager();
};