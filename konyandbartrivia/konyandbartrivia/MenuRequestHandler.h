#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "LoggedUser.h"

class Communicator;
class RoomManager;
class LoggedUser;
class IRequestHandler;
class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler {
public:
	MenuRequestHandler(LoggedUser* user, RequestHandlerFactory& handlerFactory);
	virtual ~MenuRequestHandler();

	virtual bool isRequestRelevant(RequestInfo info) override;
	virtual RequestResult handleRequest(RequestInfo info) override;
private:
	RequestHandlerFactory& m_handlerFactory;
	LoggedUser* m_user;
	RoomManager& m_roomManager;

	virtual RequestResult signOut(RequestInfo info);
	virtual RequestResult getRooms(RequestInfo info);
	virtual RequestResult getPlayersInRoom(RequestInfo info);
	virtual RequestResult joinRoom(RequestInfo info);
	virtual RequestResult createRoom(RequestInfo info);
};