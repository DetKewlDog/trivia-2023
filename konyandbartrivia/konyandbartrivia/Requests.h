#pragma once
#include <string>

typedef struct LoginRequest {
	std::string username;
	std::string password;
} LoginRequest;

typedef struct SignUpRequest {
	std::string username;
	std::string password;
	std::string email;
} SignUpRequest;

typedef struct GetPlayersInRoomRequest {
	unsigned int roomId;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest {
	unsigned int roomId;
} JoinRoomRequest;

typedef struct CreateRoomRequest {
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answersTimeout;
} CreateRoomRequest;

typedef struct SubmitAnswerRequest {
	unsigned int answerId;
} SubmitAnswerRequest;