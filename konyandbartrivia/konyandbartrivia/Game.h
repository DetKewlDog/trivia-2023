#pragma once
#include <vector>
#include <map>
#include "GameData.h"
#include "LoggedUser.h"
#include <chrono>
#include <thread>
#include "PlayerResults.h"
#include <iostream>

class LoggedUser;

class Game {
private:
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
	unsigned int m_answerTimeout;
	std::chrono::steady_clock::time_point startTimestamp;
public:
	unsigned int m_gameId;
	
	Game(std::vector<Question> questions, std::vector<LoggedUser> users, unsigned int gameId, unsigned int answerTimeout);
	~Game() = default;
	std::pair<Question, int> getQuestionForUser(LoggedUser user);
	bool submitAnswer(LoggedUser user, unsigned int answerId);
	void removePlayer(LoggedUser user);
	std::vector<PlayerResults> getPlayerResults();
	bool operator==(const Game& other) const;
};