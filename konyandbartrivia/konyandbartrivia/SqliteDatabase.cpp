#include "SqliteDatabase.h"

int callbackInt(void* _data, int argc, char** argv, char** azColName);
int callbackStats(void* _data, int argc, char** argv, char** azColName);
int callbackQuestion(void* _data, int argc, char** argv, char** azColName);

SqliteDatabase::SqliteDatabase() {
	this->open();
}

SqliteDatabase::~SqliteDatabase() {
	this->close();
}

bool SqliteDatabase::open() {
	int file_exist = _access("DB.db", 0);
	int res = sqlite3_open("DB.db", &this->db);
	if (res != SQLITE_OK) {
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	return true;

}
bool SqliteDatabase::close() {
	sqlite3_close(db);
	this->db = nullptr;
	return true;
}
//returns 1 if user is found
int SqliteDatabase::doesUserExist(std::string username) {
	executeSQL("SELECT 1 FROM USERS WHERE Username = '"
		+ username +
		"';", callbackInt);
	return result;
}

//checks if username and password match
int SqliteDatabase::doesPasswordMatch(std::string username, std::string password) {
	if (!doesUserExist(username)) return 0;
	executeSQL("SELECT 1 FROM USERS WHERE Username = '"
		+ username + 
		"' AND Password = '" +
		password + "';", callbackInt);
	return result;
}

int SqliteDatabase::addNewUser(std::string username, std::string password, std::string email) {
	if (this->doesUserExist(username)) return false; //if user exists it doesn't add user to the sql
	executeSQL("INSERT INTO USERS (Username, Password, Email, Wins, TotalGames) VALUES ('" + username +
		"', '" + password +
		"', '" + email +
		"', 0, 0);"); //inserts values
	return true;
}

std::vector<Question> SqliteDatabase::getQuestions(int count) {
	executeSQL("SELECT * FROM QUESTIONS ORDER BY RANDOM() LIMIT " + std::to_string(count), callbackQuestion);
	return questions;
}

std::pair<int, int> SqliteDatabase::getUserStatistics(std::string username) {
	executeSQL("SELECT Wins, TotalGames FROM USERS WHERE USERNAME = '" + username + "'", callbackStats);
	return stats;
}
int SqliteDatabase::setUserStatistics(std::string username, int winsCount, int totalGamesCount) {
	if (!this->doesUserExist(username)) return false; //if user doesn't exist it doesn't modify their stats
	executeSQL("UPDATE USERS SET Wins = " + std::to_string(winsCount) +
		", TotalGames = " + std::to_string(totalGamesCount) +
		" WHERE Username = '" + username +
		"';"); //inserts values
	return true;
}

int callbackInt(void* _data, int argc, char** argv, char** azColName) {
	*static_cast<int*>(_data) = atoi(argv[0]);
	return 0; //puts data into data pointer.
}

int callbackStats(void* _data, int argc, char** argv, char** azColName) {
	std::cout << argv[0] << " " << argv[1] << std::endl;
	*static_cast<std::pair<int, int>*>(_data) = { atoi(argv[0]), atoi(argv[1]) };
	return 0; //puts data into data pointer.
}

int callbackQuestion(void* _data, int argc, char** argv, char** azColName) {
	auto& lst = *static_cast<std::vector<Question>*>(_data);

	std::vector<std::string> answers = JsonRequestPacketDeserializer::deserializeStringVector(argv[2]);
	answers.push_back(argv[1]);
	std::random_shuffle(answers.begin(), answers.end());
	int index = std::distance(answers.begin(), std::find(answers.begin(), answers.end(), argv[1]));

	lst.push_back(Question{ 
		std::string(argv[0]), 
		answers,
		index
	});
	return 0; //puts data into data pointer.
}

bool SqliteDatabase::executeSQL(const std::string query, int (*callbackFunc)(void*, int, char**, char**)) {
	char* errmsg = nullptr;
	bool SQL_OK = false;
	void* ret = nullptr;
	if (callbackFunc == callbackInt) {
		ret = &result;
		result = 0;
	}
	else if (callbackFunc == callbackQuestion) {
		ret = &questions;
		questions.clear();
	}
	else if (callbackFunc == callbackStats) {
		ret = &stats;
	}
	SQL_OK = sqlite3_exec(db, query.c_str(), callbackFunc, ret, &errmsg) == SQLITE_OK;

	if (errmsg != nullptr) throw std::exception(("SQL ERROR! " + std::string(errmsg)).c_str());
	return SQL_OK;
}