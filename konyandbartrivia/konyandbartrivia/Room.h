#pragma once
#include <vector>
#include "RoomData.h"
#include "LoggedUser.h"

class LoggedUser;

class Room {
private:
	RoomData m_metadata;
public:
	std::vector<LoggedUser> m_users;
	
	Room(RoomData data) : m_metadata(data) { }
	~Room() = default;
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	std::vector<std::string> getAllUsers();
	RoomData& getMetadata();
};