#include "RoomAdminRequestHandler.h"
#include <iostream>

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser* user, Room* room, RequestHandlerFactory& handlerFactory) : m_handlerFactory(handlerFactory), m_user(user), m_room(room), m_roomManager(handlerFactory.getRoomManager()) {

}

RoomAdminRequestHandler::~RoomAdminRequestHandler() {
	this->m_roomManager.deleteRoom(m_room->getMetadata().id);
	this->m_handlerFactory.m_loginManager.logout(this->m_user->getUsername());
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo info) {
	return info.id == RequestIds::CloseRoomId
		|| info.id == RequestIds::StartGameId
		|| info.id == RequestIds::GetRoomStateId;
}
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo info) {
	if (info.id == RequestIds::CloseRoomId) return closeRoom(info);
	if (info.id == RequestIds::StartGameId) return startGame(info);
	return getRoomState(info);
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo info) {
	this->m_roomManager.deleteRoom(m_room->getMetadata().id);
	return {
		JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse{ 1 }),
		m_handlerFactory.createMenuRequestHandler(this->m_user)
	};
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo info) {
	Room* room = this->m_roomManager.getRoom(m_room->getMetadata().id);
	RoomData& roomData = room->getMetadata();
	roomData.isActive = true;
	m_handlerFactory.getGameManager().createGame(*room);
	Game* game = m_handlerFactory.getGameManager().getGame(roomData.id);
	return {
		JsonResponsePacketSerializer::serializeResponse(StartGameResponse{ 1 }),
		m_handlerFactory.createGameRequestHandler(game, this->m_user)
	};
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo info) {
	RoomData roomData = m_room->getMetadata();
	return {
		JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{ 
			1, 
			roomData.isActive == 1, 
			m_room->getAllUsers(), 
			roomData.numOfQuestionsInGame, 
			roomData.timePerQuestion, 
			roomData.maxPlayers
		}),
		this
	};
}