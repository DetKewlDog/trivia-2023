#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <io.h>
#include <iostream>
#include "Communicator.h"
#include "JsonRequestPacketDeserializer.h"

class Communicator;

class SqliteDatabase : public IDatabase {
public:
	SqliteDatabase();
	virtual ~SqliteDatabase();

	virtual bool open() override;
	virtual bool close() override;

	virtual int doesUserExist(std::string username) override;
	virtual int doesPasswordMatch(std::string username, std::string password) override;
	virtual int addNewUser(std::string username, std::string password, std::string email) override;
	virtual std::vector<Question> getQuestions(int count) override;
	virtual std::pair<int, int> getUserStatistics(std::string username) override;
	virtual int setUserStatistics(std::string username, int winsCount, int totalGamesCount) override;
private:
	sqlite3* db;
	int result;
	std::vector<Question> questions;
	std::pair<int, int> stats;

	bool executeSQL(const std::string query, int (*callbackFunc)(void*, int, char**, char**) = nullptr);
};