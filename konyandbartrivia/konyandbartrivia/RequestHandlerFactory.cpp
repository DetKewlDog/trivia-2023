#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* database) : m_loginManager(database), m_gameManager(database) {
	this->m_database = database;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler() {
	return new LoginRequestHandler(*this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser* user) {
	return new MenuRequestHandler(user, *this);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser* user, Room* room) {
	return new RoomAdminRequestHandler(user, room, *this);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser* user, Room* room) {
	return new RoomMemberRequestHandler(user, room, *this);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Game* game, LoggedUser* user) {
	return new GameRequestHandler(game, user, *this);
}

LoginManager& RequestHandlerFactory::getLoginManager() {
	return this->m_loginManager;
}

RoomManager& RequestHandlerFactory::getRoomManager() {
	return this->m_roomManager;
}

GameManager& RequestHandlerFactory::getGameManager() {
	return this->m_gameManager;
}