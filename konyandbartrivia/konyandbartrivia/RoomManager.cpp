#include "RoomManager.h"
#include <iostream>

void RoomManager::createRoom(LoggedUser user, RoomData roomData) {
	Room newRoom(roomData);
	newRoom.addUser(user);
	this->m_rooms.insert({ roomData.id, newRoom });
}
void RoomManager::deleteRoom(int ID) {
	auto it = this->m_rooms.find(ID);
	if (it != this->m_rooms.end()) {
		this->m_rooms.erase(it);
	}
}
unsigned int RoomManager::getRoomState(int ID) {
	auto it = this->m_rooms.find(ID);
	if (it == this->m_rooms.end()) return 0;
	return it->second.getMetadata().isActive;
}
std::vector<RoomData> RoomManager::getRooms() {
	std::vector<RoomData> result;
	for (auto room : this->m_rooms) {
		result.push_back(room.second.getMetadata());
	}
	return result;
}
Room* RoomManager::getRoom(int ID) {
	auto it = this->m_rooms.find(ID);
	if (it == this->m_rooms.end()) return nullptr;
	return &it->second;
}