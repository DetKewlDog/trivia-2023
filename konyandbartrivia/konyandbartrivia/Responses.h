#pragma once
#include "PlayerResults.h"
#include "RoomData.h"
#include <vector>

typedef struct ErrorResponse {
	std::string message;
} ErrorResponse;

typedef struct LoginResponse {
	unsigned int status;
} LoginResponse;

typedef struct SignUpResponse {
	unsigned int status;
} SignUpResponse;

typedef struct LogoutResponse {
	unsigned int status;
} LogoutResponse;

typedef struct GetRoomsResponse {
	unsigned int status;
	std::vector<RoomData> rooms;
	unsigned int winsCount;
	unsigned int totalGamesCount;
} GetRoomsResponse;

typedef struct GetPlayersInRoomResponse {
	std::vector<std::string> players;
} GetPlayersInRoomResponse;

typedef struct JoinRoomResponse {
	unsigned int status;
} JoinRoomResponse;

typedef struct CreateRoomResponse {
	unsigned int status;
} CreateRoomResponse;

typedef struct CloseRoomResponse {
	unsigned int status;
} CloseRoomResponse;

typedef struct StartGameResponse {
	unsigned int status;
} StartGameResponse;

typedef struct GetRoomStateResponse {
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
	unsigned int maxPlayers;
} GetRoomStateResponse;

typedef struct LeaveRoomResponse {
	unsigned int status;
} LeaveRoomResponse;

typedef struct LeaveGameResponse {
	unsigned int status;
} LeaveGameResponse;

typedef struct GetQuestionResponse {
	unsigned int status;
	std::string question;
	std::vector<std::string> answers;
} GetQuestionResponse;

typedef struct SubmitAnswerResponse {
	unsigned int status;
	unsigned int correctAnswerId;
} SubmitAnswerResponse;

typedef struct GetGameResultsResponse {
	unsigned int status;
	std::vector<PlayerResults> results;
} GetGameResultsResponse;