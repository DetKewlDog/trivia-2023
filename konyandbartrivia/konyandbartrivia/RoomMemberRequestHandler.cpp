#include "RoomMemberRequestHandler.h"
#include <iostream>

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser* user, Room* room, RequestHandlerFactory& handlerFactory) : m_handlerFactory(handlerFactory), m_user(user), m_room(room), m_roomManager(handlerFactory.getRoomManager()) {

}

RoomMemberRequestHandler::~RoomMemberRequestHandler() {
	this->m_room->removeUser(*this->m_user);
	this->m_handlerFactory.m_loginManager.logout(this->m_user->getUsername());
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo info) {
	return info.id == RequestIds::LeaveRoomId
		|| info.id == RequestIds::GetRoomStateId
		|| info.id == RequestIds::StartGameId;
}
RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo info) {
	if (info.id == RequestIds::StartGameId) {
		Room* room = this->m_roomManager.getRoom(m_room->getMetadata().id);
		RoomData roomData = room->getMetadata();
		m_handlerFactory.getGameManager().createGame(*room);
		Game* game = m_handlerFactory.getGameManager().getGame(roomData.id);
		return {
			JsonResponsePacketSerializer::serializeResponse(StartGameResponse{ 1 }),
			m_handlerFactory.createGameRequestHandler(game, this->m_user)
		};
	}
	try {
		int check = (m_room == nullptr);
		return info.id == RequestIds::LeaveRoomId ? leaveRoom(info) : getRoomState(info);
	}
	catch(...) {
		return {
			JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "You are not in a room!" }),
			m_handlerFactory.createMenuRequestHandler(this->m_user)
		};
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo info) {
	Room* room = this->m_roomManager.getRoom(m_room->getMetadata().id);
	room->removeUser(*this->m_user);
	return {
		JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse{ 1 }),
		m_handlerFactory.createMenuRequestHandler(this->m_user)
	};
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo info) {
	try {
		Room* room = this->m_roomManager.getRoom(m_room->getMetadata().id);
		if (room == nullptr) {
			return {
				JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "You are not in a room!" }),
				m_handlerFactory.createMenuRequestHandler(this->m_user)
			};
		}
		RoomData roomData = room->getMetadata();
		IRequestHandler* newRequest = this;
		if (roomData.isActive) {
			m_handlerFactory.getGameManager().createGame(*room);
			Game* game = m_handlerFactory.getGameManager().getGame(roomData.id);
			newRequest = m_handlerFactory.createGameRequestHandler(game, m_user);
		}
		return {
			JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse{
				1,
				roomData.isActive == 1,
				m_room->getAllUsers(),
				roomData.numOfQuestionsInGame,
				roomData.timePerQuestion,
				roomData.maxPlayers
			}),
			this
		};
	}
	catch (...) {
		return {
			JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "You are not in a room!" }),
			m_handlerFactory.createMenuRequestHandler(this->m_user)
		};
	}
}