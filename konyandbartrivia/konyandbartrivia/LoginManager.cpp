#include "LoginManager.h"

LoginManager::LoginManager(IDatabase* database) {
	this->m_database = database;
}

LoggedUser* LoginManager::signUp(std::string username, std::string password, std::string email) {
	LoggedUser* user = new LoggedUser(username);
	if (std::find(m_loggedUsers.begin(), m_loggedUsers.end(), *user) != m_loggedUsers.end()) return nullptr;
	if (!m_database->addNewUser(username, password, email)) return nullptr;
	m_loggedUsers.push_back(*user);
	return user;
}
LoggedUser* LoginManager::login(std::string username, std::string password) {
	LoggedUser* user = new LoggedUser(username);
	if (std::find(m_loggedUsers.begin(), m_loggedUsers.end(), *user) != m_loggedUsers.end()) return nullptr;
	if (!this->m_database->doesPasswordMatch(username, password)) return nullptr;
	m_loggedUsers.push_back(*user);
	return user;
}
bool LoginManager::logout(std::string username) {
	LoggedUser* user = new LoggedUser(username);
	auto loggeduser = std::find(m_loggedUsers.begin(), m_loggedUsers.end(), *user);
	if (loggeduser == this->m_loggedUsers.end()) return false;
	m_loggedUsers.erase(loggeduser);
	delete user;
	return true;
}