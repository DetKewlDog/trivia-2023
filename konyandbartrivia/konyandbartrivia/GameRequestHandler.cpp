#include "GameRequestHandler.h"
#include <iostream>

GameRequestHandler::GameRequestHandler(Game* game, LoggedUser* user, RequestHandlerFactory& handlerFactory) : m_handlerFactory(handlerFactory), m_user(user), m_game(game), m_gameManager(handlerFactory.getGameManager()), m_question("", std::vector<std::string>(), -1) {

}

GameRequestHandler::~GameRequestHandler() {
	this->m_gameManager.deleteGame(m_game->m_gameId);
	this->m_handlerFactory.m_gameManager.deleteGame(m_game->m_gameId);
	this->m_handlerFactory.m_roomManager.deleteRoom(m_game->m_gameId);
}

bool GameRequestHandler::isRequestRelevant(RequestInfo info) {
	return info.id == RequestIds::GetQuestionId
		|| info.id == RequestIds::SubmitAnswerId
		|| info.id == RequestIds::GetGameResultsId
		|| info.id == RequestIds::LeaveGameId
		|| info.id == RequestIds::GetRoomStateId;
}
RequestResult GameRequestHandler::handleRequest(RequestInfo info) {
	if (info.id == RequestIds::GetQuestionId) return getQuestion(info);
	if (info.id == RequestIds::SubmitAnswerId) return submitAnswer(info);
	if (info.id == RequestIds::GetGameResultsId) return getGameResults(info);
	if (info.id == RequestIds::GetRoomStateId) return getRoomState(info);
	return leaveGame(info);
}

RequestResult GameRequestHandler::getQuestion(RequestInfo info) {
	std::pair<Question, int> res = this->m_game->getQuestionForUser(*m_user);
	m_question = res.first;
	unsigned int questionId = res.second;
	return {
		JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse{
			questionId + 1,
			m_question.getQuestion(),
			m_question.getPossibleAnswers()
		}),
		this
	};
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo info) {
	SubmitAnswerRequest request = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(info.buffer);
	bool isCorrect = this->m_game->submitAnswer(*m_user, request.answerId);
	return {
		JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse{ 
			isCorrect,
			(unsigned int)m_question.getCorrectAnswerId()
		}),
		this
	};
}

RequestResult GameRequestHandler::getGameResults(RequestInfo info) {
	std::pair<unsigned int, unsigned int> stats = m_handlerFactory.m_database->getUserStatistics(m_user->getUsername());
	std::vector<PlayerResults> results = m_game->getPlayerResults();
	std::sort(results.begin(), results.end(), [](const PlayerResults& a, const PlayerResults& b) {
		// Sort in descending order (from highest to lowest)
		return a.correctAnswerCount > b.correctAnswerCount;
	});
	if (results.begin()->username == m_user->getUsername()) {
		stats.first++;
	}
	m_handlerFactory.m_database->setUserStatistics(m_user->getUsername(), stats.first, stats.second + 1);
	
	return {
		JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse{ 1, m_game->getPlayerResults() }),
		this
	};
}

RequestResult GameRequestHandler::leaveGame(RequestInfo info) {
	this->m_game->removePlayer(*this->m_user);
	return {
		JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse{ 1 }),
		m_handlerFactory.createMenuRequestHandler(this->m_user)
	};
}

RequestResult GameRequestHandler::getRoomState(RequestInfo info) {
	Room* room = m_handlerFactory.getRoomManager().getRoom(m_game->m_gameId);
	RoomData roomData = room->getMetadata();
	return {
		JsonResponsePacketSerializer::serializeResponse({
			1,
			true,
			room->getAllUsers(),
			roomData.numOfQuestionsInGame,
			roomData.timePerQuestion,
			roomData.maxPlayers
		}),
		this
	};
}