#pragma once
#include <map>
#include <thread>
#include <string>
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "Helper.h"
#include "RequestHandlerFactory.h"

class Server;
class IDatabase;
class RequestHandlerFactory;

class Communicator {
private:
	int _port;
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;

	void bindAndListen();
	void handleNewClient(SOCKET sock);

public:
	Communicator(int port, RequestHandlerFactory& handlerFactory);
	~Communicator();
	void startHandleRequests();
};