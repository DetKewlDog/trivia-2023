#pragma comment(lib, "Ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>


int main() {
	WSAInitializer wsaInit;
	try {
		Server server(8826);
		server.run();
	}
	catch (std::exception& e) {
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	return 0;

}