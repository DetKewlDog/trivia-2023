#pragma once
#include <string>
#include "Responses.h"
#include "json.hpp"

using json = nlohmann::json;

class JsonResponsePacketSerializer {
public:
	static std::vector<unsigned char> serializeResponse(ErrorResponse response);
	static std::vector<unsigned char> serializeResponse(LoginResponse response);
	static std::vector<unsigned char> serializeResponse(SignUpResponse response);
	static std::vector<unsigned char> serializeResponse(LogoutResponse response);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse response);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse response);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse response);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse response);
	static std::vector<unsigned char> serializeResponse(CloseRoomResponse response);
	static std::vector<unsigned char> serializeResponse(StartGameResponse response);
	static std::vector<unsigned char> serializeResponse(GetRoomStateResponse response);
	static std::vector<unsigned char> serializeResponse(LeaveRoomResponse response);
	static std::vector<unsigned char> serializeResponse(LeaveGameResponse response);
	static std::vector<unsigned char> serializeResponse(GetQuestionResponse response);
	static std::vector<unsigned char> serializeResponse(SubmitAnswerResponse response);
	static std::vector<unsigned char> serializeResponse(GetGameResultsResponse response);

	static void serializeRoomDataList(std::vector<RoomData> lst, json& json);
	static void serializePlayerResultsList(std::vector<PlayerResults> lst, json& json);
};