#pragma once
#include "Question.h"
#include <chrono>

typedef struct GameData {
	GameData(std::chrono::steady_clock::time_point timestamp) : currentQuestion("", std::vector<std::string>(), -1), currentQuestionId(-1), correctAnswerCount(0), wrongAnswerCount(0), averageAnswerTime(0), startTimestamp(timestamp) {
	
	}
	Question currentQuestion;
	unsigned int currentQuestionId;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
	std::chrono::steady_clock::time_point startTimestamp;
} GameData;