#include <exception>
#include "Communicator.h"

Communicator::Communicator(int port, RequestHandlerFactory& handlerFactory) : m_handlerFactory(handlerFactory) {
	this->m_serverSocket = 0;
	this->_port = port;
}

Communicator::~Communicator() {
	try {  closesocket(m_serverSocket); }
	catch (...) { }
}

void Communicator::startHandleRequests() {
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_serverSocket == INVALID_SOCKET) {
		throw std::exception(__FUNCTION__ " - socket");
	}
	this->bindAndListen();
}

void Communicator::bindAndListen() {
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(_port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR) {
		throw std::exception(__FUNCTION__ " - bind");
	}

	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR) {
		throw std::exception(__FUNCTION__ " - listen");
	}

	SOCKET client_socket;
	while (true) {
		client_socket = accept(m_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__ " - accept");

		std::thread user(&Communicator::handleNewClient, this, client_socket);
		user.detach();
		m_clients.insert({ client_socket, m_handlerFactory.createLoginRequestHandler() });
	}
}

void Communicator::handleNewClient(SOCKET sock) {
	try {
		std::cout << "new client connected: " << sock << std::endl;
		while (true) {
			IRequestHandler* request = m_clients.at(sock);
			RequestInfo info = Helper::receiveData(sock);
			std::cout << sock 
				<< " (" << request->name() << ") -> " 
				<< info.id << " " << std::string(info.buffer.begin(), info.buffer.end()) << std::endl;
			RequestResult result = request->isRequestRelevant(info) ?
				request->handleRequest(info) :
				RequestResult { 
					JsonResponsePacketSerializer::serializeResponse(ErrorResponse { "Unknown code" }), 
					request 
				};
			m_clients.at(sock) = result.newHandler;

			std::cout << sock
				<< " (" << result.newHandler->name() << ") <- "
				<< std::string(result.response.begin(), result.response.end()) << std::endl;
			Helper::sendData(sock, std::string(result.response.begin(), result.response.end()));
			m_clients[sock] = result.newHandler;
		}
	}
	catch (std::exception& e) {
		delete m_clients.at(sock);
		m_clients.erase(sock);
		closesocket(sock);
		std::cout << "Error occured: " << e.what() << std::endl;
	}
}