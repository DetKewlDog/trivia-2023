from flask import Flask, redirect
import threading
import app as App

mainApp = Flask("mainApp")
port = 5000

@mainApp.route('/')
def index():
    global port
    port += 1
    t = threading.Thread(target=App.run, args=[port])
    t.start()
    return redirect(f'http://127.0.0.1:{port}', code=302)

if __name__ == "__main__":
    mainApp.run()