from flask import Flask, render_template, request, redirect, url_for, session
from client import Client
import time

app = Flask(__name__)

username = ''
clients = dict()

def get_client(request):
    global clients
    return clients[request.environ.get('SERVER_PORT')]

@app.route('/')
def index():
    global clients
    clients[request.environ.get('SERVER_PORT')] = Client()
    return render_template('index.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    global username
    if request.method == 'GET':
        return render_template('login.html')
    username = request.form['inputUsername']
    password = request.form['inputPassword']
    output = get_client(request).login(username, password)
    if 'error' in output or output['status'] == 0:
        return render_template('login.html', error='Invalid info, please try again!')
    return redirect(url_for('menu', username=username))


@app.route('/register', methods=['POST', 'GET'])
def register():
    global username
    if request.method == 'GET':
        return render_template('register.html')
    username = request.form['inputUsername']
    password = request.form['inputPassword']
    email = request.form['inputEmail']
    output = get_client(request).register(username, password, email)
    if 'error' in output or output['status'] == 0:
        return render_template('register.html', error='Invalid info, please try again!')
    return redirect(url_for('menu', username=username))


@app.route('/menu')
def menu():
    global username
    username = request.args.get('username', '')
    rooms = get_client(request).get_rooms()
    wins = rooms['winsCount']
    total_games = rooms['totalGamesCount']
    if total_games == 0:
        win_percentage = 0
    else:
        win_percentage = wins / total_games * 100
    rooms = [room for room in rooms['rooms'] if not room['isActive']] if 'rooms' in rooms else []
    return render_template('menu.html', rooms=rooms, username=username, wins=wins, total_games=total_games, win_percentage=win_percentage)


@app.route('/room/<int:room_id>/<room_name>/<is_admin>')
def room(room_id, room_name, is_admin):
    is_admin = eval(is_admin)
    username = request.args.get('username', '')
    if not is_admin:
        get_client(request).join_room(room_id)

    room_state = get_client(request).get_room_state()
    if 'error' in room_state:
        return redirect(url_for('menu', username=username))
    elif room_state['hasGameBegun'] == True:
        return redirect(url_for('game',
            game_id=room_id,
            time_per_question=room_state['answerTimeout'],
            question_count=room_state['questionCount'],
            question_id=0,
            submitted=-1,
            is_correct=False
        ))

    players = room_state['players']
    playerCount = len(players)
    maxPlayers = room_state['maxPlayers']
    return render_template('room.html',
        room_id=room_id,
        room_name=room_name,
        players=players,
        playerCount=playerCount,
        maxPlayers=maxPlayers,
        is_admin=is_admin,
        time_per_question=room_state['answerTimeout'],
        question_count=room_state['questionCount']
    )


@app.route('/newroom', methods=['POST', 'GET'])
def newroom():
    if (request.method == 'GET'):
        return render_template('newroom.html')

    name = request.form['name']
    maxPlayers = int(request.form['maxPlayers'])
    numOfQuestionsInGame = int(request.form['numOfQuestionsInGame'])
    timePerQuestion = int(request.form['timePerQuestion'])
    output = get_client(request).create_room(name, maxPlayers, numOfQuestionsInGame, timePerQuestion)

    if 'error' in output or output['status'] == 0:
        return render_template('register.html', error='Invalid settings, please try again!')

    return redirect(url_for('room',
        room_id=output["status"],
        room_name=name,
        is_admin=True,
        time_per_question=timePerQuestion
    ))


@app.route('/leaveroom/<is_admin>')
def leaveroom(is_admin):
    is_admin = eval(is_admin)
    if is_admin: get_client(request).close_room()
    else: get_client(request).leave_room()
    return redirect(url_for('menu', username=username))


@app.route('/game/<int:game_id>/<int:time_per_question>/<int:question_count>/<submitted>/<is_correct>', methods=['GET'])
def game(game_id, time_per_question, question_count, submitted, is_correct):
    if submitted == '-1':
        get_client(request).start_game()
        submitted = False
    else:
        submitted = eval(submitted)
    is_correct = eval(is_correct)
    result = {}
    while result == {}:
        time.sleep(0.5)
        result = get_client(request).get_question()
    return render_template('game.html',
        game_id = game_id,
        question=result['question'],
        answers=result['answers'],
        question_count=question_count,
        question_id=result['status'],
        time_per_question=time_per_question,
        submitted=submitted,
        is_correct=is_correct
    )


@app.route('/submitanswer/<int:answer_id>/<int:game_id>/<int:time_per_question>/<int:question_count>/<int:question_id>')
def submitanswer(answer_id, game_id, time_per_question, question_count, question_id):
    result = get_client(request).submit_answer(answer_id)
    is_correct = result['correctAnswerId'] == answer_id
    if question_count == question_id:
        return redirect(url_for('gameresults'))
    return redirect(url_for('game',
        game_id = game_id,
        time_per_question=time_per_question,
        question_count=question_count,
        submitted=True,
        is_correct=is_correct
    ))

@app.route('/gameresults')
def gameresults():
    results = get_client(request).get_game_results()['results']
    results.sort(key=lambda x: x['correctAnswerCount'], reverse=True)
    return render_template('gameresults.html', results=results)

@app.route('/leavegame')
def leavegame():
    get_client(request).leave_game()
    return redirect(url_for('menu', username=username))

def run(port):
    app.run(port=port)